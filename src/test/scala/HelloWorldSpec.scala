import org.scalatest.{Matchers, WordSpecLike}

class HelloWorldSpec extends WordSpecLike with Matchers {

  "Hello world" should {
    "say hello world" in {
       HelloWorld.sayIt("Hello World") should equal ("Hello World")
    }
  }

}
